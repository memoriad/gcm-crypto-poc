import { Component } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import crypto from 'crypto-browserify';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  ALGO: crypto.CipherGCMTypes = 'aes-256-gcm';
  BLOCK_SIZE_BYTES: number = 12;
  TAG_SIZE_BYTES: number = 16;

  cipherKey: string;

  plainText: string;
  encryptText: string;

  encrypted: string;
  decrypted: string;

  constructor() {
  }

  encryptGCM() { 
    let iv = crypto.randomBytes(this.BLOCK_SIZE_BYTES);
    console.log("iv: ", iv.toString('base64'))
    let cipher = crypto.createCipheriv(this.ALGO, this.cipherKey, iv);

    let enc = cipher.update(this.plainText, 'utf8', 'base64');
    enc += cipher.final('base64');
    console.log("enc: ", enc)

    let tag = cipher.getAuthTag()
    console.log("tag: ", tag.toString('base64'))
    enc = Buffer.concat([iv, Buffer.from(enc, 'base64'), tag]);

    this.encrypted = enc.toString('base64');
  }

  decryptGCM() {
    this.decrypted = '';
    
    let decoded = Buffer.from(this.encryptText, 'base64');

    let iv = decoded.slice(0, this.BLOCK_SIZE_BYTES);
    let cipherText = decoded.slice(this.BLOCK_SIZE_BYTES, decoded.length - this.TAG_SIZE_BYTES);
    let tag = decoded.slice(decoded.length - this.TAG_SIZE_BYTES);

    let decipher = crypto.createDecipheriv(this.ALGO, this.cipherKey, iv);
    decipher.setAuthTag(tag);
    
    let str = decipher.update(cipherText, null, 'utf8');
    str += decipher.final('utf8');

    this.decrypted = str
  }

  encryptUsingAES256() {
    let _key = CryptoJS.enc.Utf8.parse(this.cipherKey);
    let _iv = CryptoJS.enc.Utf8.parse(this.cipherKey);

    this.encrypted = CryptoJS.AES.encrypt(
      JSON.stringify(this.plainText), _key, {
      keySize: 32,
      iv: _iv,
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    }).toString();
  }

  decryptUsingAES256() {
    let _key = CryptoJS.enc.Utf8.parse(this.cipherKey);
    let _iv = CryptoJS.enc.Utf8.parse(this.cipherKey);

    this.decrypted = CryptoJS.AES.decrypt(
      this.encryptText, _key, {
      keySize: 32,
      iv: _iv,
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    }).toString(CryptoJS.enc.Utf8);
  }
}